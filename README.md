# WORK TIME
Next.js auth UI implementation according design: https://www.figma.com/file/SBazVYjj8VfrfKUQpUO7Zq/Test-task-for-Front-end-Developer?type=design&node-id=22-1326&mode=design&t=xJ1Qyis9Hvm3JsPc-0

## Dev 
To start app in container, use 

    docker-compose up

App will be available on http://localhost:7777