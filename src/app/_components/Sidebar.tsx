import styles from "../_styles/sidebar.module.css";
import { Nav } from "@/app/auth/sign-up/_components/Nav";

export default function Sidebar() {
  return (
    <>
      <div className={`${styles.sidebar}`}>
        <Nav />
      </div>
    </>
  );
}
