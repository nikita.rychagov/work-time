import "../_styles/footer.css";

export default function Footer({ classNames }: { classNames?: string[] }) {
  return (
    <footer className={`footer ${classNames ? [...classNames].join(" ") : ""}`}>
      <span>Help@worktime.com</span>
    </footer>
  );
}
