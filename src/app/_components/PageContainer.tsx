import { ReactNode } from "react";
import "@/app/_styles/page-container.css";

export default function PageContainer({ children }: { children: ReactNode }) {
  return <div className={"page-container"}>{children}</div>;
}
