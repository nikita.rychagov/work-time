import { ReactNode } from "react";
import styles from "../_styles/main.module.css";
import { classNamesFormatter } from "@/common/helpers";

export default function Main({
  children,
  classNameList,
}: {
  children: ReactNode;
  classNameList?: string[];
}) {
  const mainClassNames = classNamesFormatter(styles.main, classNameList);
  return (
    <>
      <main className={mainClassNames}>{children}</main>
    </>
  );
}
