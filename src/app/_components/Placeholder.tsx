import styles from "../_styles/placeholder.module.css";
import { ReactNode } from "react";

export default function Placeholder({ children }: { children: ReactNode }) {
  return <div className={styles.container}>{children}</div>;
}
