import Image from "next/image";
import Link from "next/link";
import styles from "../_styles/header.module.css";
import { Properties } from "csstype";

export default function Header({
  style,
  isAbsolute,
}: {
  style?: Properties;
  isAbsolute?: boolean;
}) {
  return (
    <>
      <header
        className={`${styles.header} ${isAbsolute ? styles.absolute : ""}`}
        style={{ ...style }}
      >
        <Link href={"/"}>
          <div className={styles.container}>
            <Image src={"/Logo.svg"} width={56} height={56} alt={"Logo"} />
            <div>
              <p className={styles.title}>Work time</p>
              <p className={styles.subtitle}>HR management</p>
            </div>
          </div>
        </Link>
      </header>
    </>
  );
}
