import { AuthGuard } from "@/common/guards";
import Placeholder from "@/app/_components/Placeholder";

export default function Home() {
  return (
    // <SessionProvider>  Uncomment when switching to actual auth process
    <AuthGuard>
      <div className={"h-screen align-middle"}>
        {/*<Header />*/}
        <Placeholder>
          <div className={"text-center relative top-[100px] m-auto"}>
            Protected page
          </div>
        </Placeholder>
      </div>
    </AuthGuard>
    // </SessionProvider>
  );
}
