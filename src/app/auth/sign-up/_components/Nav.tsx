import NavItem from "./NavItem";
import "../_styles/nav.css";

export function Nav() {
  return (
    <div className="tabs-container">
      <NavItem
        title={"You details"}
        description={"Please provide your name and email"}
        step={0}
      />
      <NavItem
        title={"Choose a password"}
        description={"Choose a secure password"}
        step={1}
      />
      <NavItem
        title={"Directions"}
        description={"Tell us what your situation is"}
        step={2}
      />
    </div>
  );
}
