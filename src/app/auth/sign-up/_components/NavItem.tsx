import { SignUpContext } from "@/app/auth/_components/SignUpContextProvider";
import { useContext, useEffect, useState } from "react";
import styles from "../_styles/nav-item.module.css";
import { classNamesFormatter } from "@/common/helpers";

enum TabState {
  SUCCESS = "success",
  CURRENT = "current",
  NEXT = "next",
}

export default function NavItem({
  title,
  description,
  step,
}: {
  title: string;
  description: string;
  step: number;
}) {
  const { currentStep, setCurrentStep } = useContext(SignUpContext);

  const changeTab = () => {
    setCurrentStep(step);
  };

  const [tabState, setTabState] = useState(TabState.CURRENT);

  useEffect(() => {
    if (currentStep === step) {
      setTabState(TabState.CURRENT);
      return;
    }
    if (currentStep > step) {
      setTabState(TabState.SUCCESS);
      return;
    }
    setTabState(TabState.NEXT);
    return;
  }, [currentStep, step]);

  const buttonClassNames = classNamesFormatter(styles.item, [], {
    "cursor-pointer": tabState !== TabState.NEXT,
  });

  return (
    <>
      <button
        className={buttonClassNames}
        onClick={changeTab}
        disabled={tabState === TabState.NEXT}
      >
        <div className={styles["icon-container"]}>
          <i className={`${styles[tabState]} ${styles.icon}`}></i>
        </div>
        <div className={styles.text}>
          <span className={styles.title}>{title}</span>
          <span className={styles.desc}>{description}</span>
        </div>
      </button>
    </>
  );
}
