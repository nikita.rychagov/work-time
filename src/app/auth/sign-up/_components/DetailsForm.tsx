import { SignUpContext } from "@/app/auth/_components/SignUpContextProvider";
import { Input, RadioButton } from "@/common/components";
import { phoneFormatter } from "@/common/helpers";
import { useContext, useState } from "react";
import { FieldValues } from "react-hook-form";
import TermsAndConditions from "./TermsAndConditions";
import "../_styles/details-form.css";
import { defaultValidators } from "@/common/constants/default-validators";
import Form from "@/app/auth/_components/Form";
import InputSection from "@/app/auth/_components/InputSection";

export default function DetailsForm({
  setDetailsFormData,
  detailsFormData,
}: {
  setDetailsFormData: (detailsFormData: FieldValues) => void;
  detailsFormData: FieldValues;
}) {
  /* Contexts */
  const {
    isCompany,
    setIsCompany,

    currentStep,
    setCurrentStep,
  } = useContext(SignUpContext);

  const [isAgreedWithTerms, setIsAgreedWithTerms] = useState<boolean>(false);

  /* Form events handlers */
  const toggleIsCompany = (event: any) => {
    setIsCompany(JSON.parse(event.target.value));
  };
  const onAgreedChange = () => {
    setIsAgreedWithTerms(!isAgreedWithTerms);
  };
  const onSubmit = (detailsFormValue: FieldValues) => {
    setDetailsFormData(detailsFormValue);
    setCurrentStep(currentStep + 1);
  };

  return (
    <Form
      defaultValues={detailsFormData}
      buttonLabel={"Next"}
      processResult={onSubmit}
      title={"Sign up"}
      description={"Please provide your name and email"}
      extraButtonDisableCondition={!isAgreedWithTerms}
    >
      <InputSection>
        <div className="name-group">
          <Input
            label={"Your name"}
            name={"firstName"}
            placeholder={"Your name"}
            validator={defaultValidators.firstName}
          />

          <Input
            name={"lastName"}
            label={"Your last name"}
            placeholder={"Your last name"}
            validator={defaultValidators.lastName}
          />
        </div>

        <Input
          name={"phone"}
          label={"Mobile Number"}
          placeholder={"+7(999)999-99-99"}
          validator={defaultValidators.phone}
          maxLength={16}
          onKeyDown={phoneFormatter}
        />

        <div className="radio-group-container flex-col">
          <span className="radio-label">Are you a company?</span>
          <div className="radio-group">
            <RadioButton
              label={"Yes"}
              name={"yes"}
              value={"true"}
              checked={isCompany}
              onChange={toggleIsCompany}
            />
            <RadioButton
              label={"No"}
              name={"no"}
              value={"false"}
              checked={!isCompany}
              onChange={toggleIsCompany}
            />
          </div>
        </div>
        {isCompany ? (
          <Input
            name={"address"}
            label={"Address"}
            placeholder={"Enter your address company"}
            validator={defaultValidators.address}
          />
        ) : null}
        <TermsAndConditions
          checked={isAgreedWithTerms}
          onAgreedChange={onAgreedChange}
        />
      </InputSection>
    </Form>
  );
}
