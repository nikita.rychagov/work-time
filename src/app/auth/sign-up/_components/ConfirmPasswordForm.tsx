import InputSection from "@/app/auth/_components/InputSection";
import PasswordInput from "@/app/auth/_components/PasswordInput";
import { defaultValidators } from "@/common/constants/default-validators";
import Form from "@/app/auth/_components/Form";
import { useContext } from "react";
import { SignUpContext } from "@/app/auth/_components/SignUpContextProvider";
import { FieldValues } from "react-hook-form";

export default function ConfirmPasswordForm({
  password,
  setPassword,
}: {
  password: string;
  setPassword: (password: string) => void;
}) {
  const { currentStep, setCurrentStep } = useContext(SignUpContext);

  const onSubmit = (passwordFormValue: FieldValues) => {
    setPassword(passwordFormValue.password);
    setCurrentStep(currentStep + 1);
  };

  return (
    <>
      <Form
        defaultValues={{ password }}
        buttonLabel={"Next"}
        processResult={onSubmit}
        title={"Sign up"}
        description={"Choose a secure password"}
      >
        <InputSection>
          <PasswordInput
            validator={defaultValidators.password}
            label={"Create a password"}
            placeholder={"*************"}
            hasVisibilityToggle
          />

          <PasswordInput
            label={"Confirm password"}
            placeholder={"Confirm password"}
            name={"confirmPassword"}
            hasMatchValidation
            validator={{
              ...defaultValidators.password,
              required: "Please, confirm your password",
            }}
          />
        </InputSection>
      </Form>
    </>
  );
}
