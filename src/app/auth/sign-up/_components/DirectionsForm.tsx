import InputSection from "@/app/auth/_components/InputSection";
import {Button} from "@/common/components";
import {ButtonTypeEnum} from "@/common/types";
import Image from "next/image";
import "../_styles/directions-form.css";
import Form from "@/app/auth/_components/Form";
import {Direction} from "@/common/constants"; // Very esoteric Form, I didn't find more proper way to handle it than to emulate behavior

// Very esoteric Form, I didn't find more proper way to handle it than to emulate behavior
export default function DirectionsForm({
  triggerSignUpSubmit,
  direction,
  setDirection,
}: {
  triggerSignUpSubmit: () => void;
  direction: Direction;
  setDirection: (direction: Direction) => void;
}) {
  const handleDirectionsInput = (direction: Direction) => {
    setDirection(direction);
  };

  return (
    // Form component is decorative here
    <Form
      buttonLabel={"Sign up"}
      processResult={triggerSignUpSubmit}
      title={"Directions"}
      classNameList={["directions"]}
      description={"Tell us what your situation is"}
    >
      <InputSection>
        <Button
          type={ButtonTypeEnum.BUTTON}
          color={direction === Direction.FREELANCER ? "primary" : "white"}
          classNamesList={["btn-fix-padding"]}
          onClick={() => handleDirectionsInput(Direction.FREELANCER)}
        >
          <div className="btn-content text-left">
            <Image src={"/Phone.svg"} width={40} height={40} alt={""} />
            <span>You are a freelancer or independent contractor</span>
          </div>
        </Button>
        <Button
          type={ButtonTypeEnum.BUTTON}
          color={direction === Direction.COMPANY ? "primary" : "white"}
          classNamesList={["btn-fix-padding"]}
          onClick={() => handleDirectionsInput(Direction.COMPANY)}
        >
          <div className="btn-content text-left">
            <Image src={"/Desktop.svg"} width={40} height={40} alt={""} />{" "}
            <span>You have a company</span>
          </div>
        </Button>
        <Button
          type={ButtonTypeEnum.BUTTON}
          color={direction === Direction.WANT_COMPANY ? "primary" : "white"}
          classNamesList={["btn-fix-padding"]}
          onClick={() => handleDirectionsInput(Direction.WANT_COMPANY)}
        >
          <div className="btn-content text-left">
            <Image src={"/Smile.svg"} width={40} height={40} alt={""} />
            <span>You want to create a company</span>
          </div>
        </Button>
      </InputSection>
    </Form>
  );
}
