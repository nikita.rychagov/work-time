import { Checkbox, PrimaryLink } from "@/common/components";
import { useFormContext } from "react-hook-form";
import "../_styles/terms-and-conditions.css";

export default function TermsAndConditions({
  onAgreedChange,
  checked,
}: {
  checked: boolean;
  onAgreedChange: () => void;
}) {
  // Get rid of this dependency?
  const { trigger } = useFormContext();

  const onCheckBoxChange = async () => {
    await trigger();
    onAgreedChange();
  };
  return (
    <label className="terms-label-container">
      <Checkbox checked={checked} onChange={onCheckBoxChange} />
      <span className="terms-label">
        I agree with all{" "}
        <PrimaryLink isNewTab={true} url={"#"}>
          Terms and Conditions
        </PrimaryLink>{" "}
        and{" "}
        <PrimaryLink isNewTab={true} url={"#"}>
          <span>Privacy Policies</span>
        </PrimaryLink>
      </span>
    </label>
  );
}
