"use client";
import SignUpContextProvider from "@/app/auth/_components/SignUpContextProvider";
import { useState } from "react";
import Main from "@/app/_components/Main";
import ConfirmPasswordForm from "@/app/auth/sign-up/_components/ConfirmPasswordForm";
import DirectionsForm from "@/app/auth/sign-up/_components/DirectionsForm";
import Footer from "@/app/_components/Footer";
import DetailsForm from "@/app/auth/sign-up/_components/DetailsForm";
import Header from "@/app/_components/Header";
import { FieldValues } from "react-hook-form";
import { useRouter } from "next/navigation";
import { Direction } from "@/common/constants";
import Sidebar from "@/app/_components/Sidebar";
import "./_styles/page-sub-container.css";
import "./_styles/sign-up-main.css";

export default function SignUp() {
  const router = useRouter();

  const [isCompany, setIsCompany] = useState<boolean>(false);
  const [currentStep, setCurrentStep] = useState<number>(0);

  const [detailsFormData, setDetailsFormData] = useState<FieldValues>({});
  const [password, setPassword] = useState<string>("");
  const [direction, setDirection] = useState<Direction>(Direction.FREELANCER);

  const onSignUpSubmit = () => {
    const resultFormData: FieldValues = {
      ...detailsFormData,
      password,
      direction,
    };

    if (!isCompany) {
      const { address, ...personFields } = resultFormData;
      console.log("Person sign up form value:", personFields);
      return router.push("/");
    }
    console.log("Company sign up form value: ", resultFormData);
    return router.push("/");
  };

  return (
    <>
      <Header />
      <SignUpContextProvider
        value={{
          isCompany,
          setIsCompany,

          currentStep,
          setCurrentStep,
        }}
      >
        <Sidebar />
        <Main classNameList={["sign-up-main"]}>
          {currentStep === 0 ? (
            <DetailsForm
              detailsFormData={detailsFormData}
              setDetailsFormData={setDetailsFormData}
            />
          ) : null}
          {currentStep === 1 ? (
            <ConfirmPasswordForm
              password={password}
              setPassword={setPassword}
            />
          ) : null}
          {currentStep === 2 ? (
            <DirectionsForm
              direction={direction}
              setDirection={setDirection}
              triggerSignUpSubmit={onSignUpSubmit}
            />
          ) : null}
          <Footer />
        </Main>
      </SignUpContextProvider>
    </>
  );
}
