"use client";
import SignInForm from "./_components/SignInForm";
import { useRouter } from "next/navigation";
import Main from "@/app/_components/Main";
import Footer from "@/app/_components/Footer";
import Header from "@/app/_components/Header";
import { FieldValues } from "react-hook-form";
import "./_styles/sign-in-main.css";

export default function SignIn() {
  const router = useRouter();

  const onSubmit = ({ phone, password }: FieldValues) => {
    console.log("Sign in request form data: ", { phone, password });
    // Proceed to protected page
    router.push("/");
    window.sessionStorage.setItem("phone", phone);
  };

  return (
    <>
      <Main classNameList={["sign-in-main"]}>
        <Header />
        <SignInForm onSignInSubmit={onSubmit} />
        <Footer classNames={["sign-in-footer"]} />
      </Main>
    </>
  );
}
