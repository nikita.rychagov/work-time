import Link from "next/link";
import { SIGN_UP_PAGE } from "@/common/constants";
import styles from "../_styles/sign-up-link.module.css";

export default function SignUpLink() {
  return (
    <div className={styles.container}>
      <span className={styles.text}>{"Don't have an account?"}</span>
      <Link className={styles.link} href={SIGN_UP_PAGE}>
        Sign up
      </Link>
    </div>
  );
}
