import InputSection from "@/app/auth/_components/InputSection";
import { Input } from "@/common/components";
import { phoneFormatter } from "@/common/helpers";
import { defaultValidators } from "@/common/constants/default-validators";
import "../_styles/sign-in-form.css";
import Form from "@/app/auth/_components/Form";
import SignUpLink from "@/app/auth/sign-in/_components/SignUpLink";
import { FieldValues } from "react-hook-form";

export default function SignInFormContent({
  onSignInSubmit,
}: {
  onSignInSubmit: (signInFormData: FieldValues) => void;
}) {
  return (
    <>
      <Form
        buttonLabel={"Sign in"}
        processResult={onSignInSubmit}
        buttonSubItem={<SignUpLink />}
        title={"Welcome back"}
        classNameList={["sign-in-form"]}
        description={"Welcome back! Please enter your details."}
      >
        <div className="forgot-password-wrapper">
          <InputSection>
            <Input
              name={"phone"}
              placeholder={"+7(999)999-99-99"}
              validator={defaultValidators.phone}
              maxLength={16}
              onKeyDown={phoneFormatter}
              label={"Mobile Number"}
            />

            <Input
              name={"password"}
              label={"Password"}
              type={"password"}
              placeholder={"Password"}
              validator={defaultValidators.password}
            />
          </InputSection>
          <a className={"forgot-link"} href="#">
            Forgot password?
          </a>
        </div>
      </Form>
    </>
  );
}
