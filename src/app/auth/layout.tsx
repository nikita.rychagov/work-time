"use client";
import { ReactNode, Suspense } from "react";
import PageContainer from "@/app/_components/PageContainer";
import { Loader } from "@/common/components";

export default function AuthPageLayout({ children }: { children: ReactNode }) {
  return (
    <>
      <PageContainer>
        <Suspense fallback={<Loader />}>{children}</Suspense>
      </PageContainer>
    </>
  );
}
