import { Input } from "@/common/components";
import { classNamesFormatter } from "@/common/helpers";
import { ReactNode, useState } from "react";
import "../_styles/password-input.css";
import { useFormContext } from "react-hook-form";

enum PasswordInputTypeEnum {
  PASSWORD = "password",
  TEXT = "text",
}

export default function PasswordInput({
  validator,
  hasVisibilityToggle = false,
  label = "Password",
  placeholder = "Password",
  name = "password",
  hasMatchValidation = false,
}: {
  children?: ReactNode;
  isDirty?: boolean;
  validator: any;
  hasVisibilityToggle?: boolean;
  label?: string;
  name?: string;
  placeholder?: string;
  hasMatchValidation?: boolean;
}) {
  const {
    getValues,
    formState: { errors },
  } = useFormContext();

  const [passButtonType, setPassButtonType] = useState<PasswordInputTypeEnum>(
    PasswordInputTypeEnum.PASSWORD
  );

  // Password visibility handling
  const switchButtonType = () => {
    const btnType =
      passButtonType === PasswordInputTypeEnum.PASSWORD
        ? PasswordInputTypeEnum.TEXT
        : PasswordInputTypeEnum.PASSWORD;
    setPassButtonType(btnType);
  };

  // Conditional CSS
  const componentClassNames = {
    visibilityBtn: classNamesFormatter("toggle-pass-visibility-btn", [], {
      visible: passButtonType === "text",
      "after-error": Boolean(errors[name]),
    }),
  };

  // Custom match validation
  const matchValidator = (match: string) =>
    match === getValues("password") || "Password doesn't match";

  const validators = {
    ...validator,
    validate: hasMatchValidation ? matchValidator : null,
  };

  return (
    <>
      <div className="password-container">
        {hasVisibilityToggle ? (
          <i
            className={componentClassNames.visibilityBtn}
            onClick={switchButtonType}
          />
        ) : null}
        <Input
          name={name}
          label={label}
          type={passButtonType}
          placeholder={placeholder}
          validator={{ ...validators }}
        />
      </div>
    </>
  );
}
