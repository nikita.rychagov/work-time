import { ReactNode } from "react";
import { FieldValues, FormProvider, useForm } from "react-hook-form";
import "@app/auth/_styles/form.css";
import { Button } from "@/common/components";
import { classNamesFormatter } from "@/common/helpers";
import FormHeader from "@/app/auth/_components/FormHeader";

export default function Form({
  children,
  defaultValues,
  processResult,
  buttonLabel,
  buttonSubItem,
  extraButtonDisableCondition = false,
  title,
  description,
  classNameList,
}: {
  children: ReactNode;
  buttonLabel: string;
  title: string;
  description: string;
  processResult: (values: FieldValues) => any;
  defaultValues?: FieldValues;
  buttonSubItem?: ReactNode;
  extraButtonDisableCondition?: boolean;
  classNameList?: string[];
}) {
  const { ...methods } = useForm({
    defaultValues,
    reValidateMode: "onBlur",
  });

  const formClassNames = classNamesFormatter("form", classNameList);

  const onSubmit = (formData: FieldValues) => {
    processResult(formData);
    methods.reset();
  };

  return (
    <>
      <FormProvider {...methods}>
        <form
          className={formClassNames}
          onSubmit={methods.handleSubmit(onSubmit)}
        >
          <FormHeader title={title} description={description} />
          {children}
          <div className={"button-wrapper"}>
            <Button
              disabled={
                extraButtonDisableCondition || !methods.formState.isValid
              }
            >
              {buttonLabel}
            </Button>
            {buttonSubItem}
          </div>
        </form>
      </FormProvider>
    </>
  );
}
