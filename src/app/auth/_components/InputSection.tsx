import { ReactNode } from "react";
import styles from "../_styles/input-section.module.css";

export default function InputSection({ children }: { children: ReactNode }) {
  return <div className={styles.section}>{children}</div>;
}
