"use client";
import { createContext, ReactNode } from "react";

export interface ISignUpContext {
  isCompany: boolean;
  setIsCompany: (isCompany: boolean) => void;

  currentStep: number;
  setCurrentStep: (currentStep: number) => void;
}

export const SignUpContext = createContext<ISignUpContext>(
  {} as ISignUpContext
);

export default function SignUpContextProvider({
  children,
  value,
}: {
  children: ReactNode;
  value: ISignUpContext;
}) {
  return (
    <SignUpContext.Provider value={value}>{children}</SignUpContext.Provider>
  );
}
