"use client";

import { useRouter } from "next/navigation";
import { useEffect, useState } from "react";
import { SIGN_IN_PAGE } from "../constants";

export function AuthGuard({ children }: { children: React.ReactNode }) {
  const router = useRouter();

  const [authenticated, setAuthenticated] = useState(false);

  useEffect(() => {
    if (!window) return;
    setAuthenticated(Boolean(window.sessionStorage.getItem("phone")));
  }, [authenticated, setAuthenticated]);

  useEffect(() => {
    if (!authenticated) {
      router.push(SIGN_IN_PAGE);
    }
  }, [authenticated]);

  return authenticated ? <>{children}</> : null;
}
