export enum CompanyFlowStep {
  DETAILS = "details",
  PASSWORD = "password",
  DIRECTIONS = "directions",
}
