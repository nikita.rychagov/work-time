export enum Direction {
  FREELANCER = "freelancer",
  COMPANY = "company",
  WANT_COMPANY = "want-company",
}
