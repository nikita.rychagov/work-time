export enum SignUpFlowTypeEnum {
  PERSON = "person",
  COMPANY = "company",
}
