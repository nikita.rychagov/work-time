export const defaultValidators = {
  firstName: {
    required: "First name is required",
    pattern: {
      value: /^[A-Za-z]*$/g,
      message: "First name should contain only letters",
    },
    maxLength: {
      value: 35,
      message: "Use maximum 35 letters",
    },
    minLength: {
      value: 2,
      message: "Use minimum 2 letters",
    },
  },
  lastName: {
    required: "Last name is required",
    pattern: {
      value: /^[A-Za-z]*$/g,
      message: "Last name should contain only letters",
    },
    maxLength: {
      value: 35,
      message: "Use maximum 35 letters",
    },
    minLength: {
      value: 2,
      message: "Use minimum 2 letters",
    },
  },
  address: {
    required: "Address is required",
  },
  phone: {
    pattern: {
      value: /^[+][0-9]{1}[(][0-9]{3}[)][-s.0-9]*$/g,
      message: "Invalid phone number",
    },
    required: "Phone is required",
  },
  password: {
    required: "Password is required",
    minLength: {
      value: 5,
      message: "Password should be at least 5 characters long",
    },
    maxLength: {
      value: 40,
      message: "Use maximum 40 symbols",
    },
  },
};
