const populateClassNamesConditionally = (
  conditional: Record<string, boolean>
): string[] => {
  return Object.keys(conditional).reduce(
    (acc: string[], className: string): string[] => {
      if (conditional[className]) {
        acc.push(className);
      }
      return acc;
    },
    []
  );
};

export function classNamesFormatter(
  staticClassNames = "",
  list?: string[],
  conditional?: Record<string, boolean>
): string {
  let conditionalClasses = conditional
    ? populateClassNamesConditionally(conditional)
    : [];
  let classList = list && list.length ? list : [];

  return [staticClassNames, ...classList, ...conditionalClasses]
    .join(" ")
    .trim();
}
