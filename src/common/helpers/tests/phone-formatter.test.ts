import { afterEach, beforeEach, describe, expect, test } from "@jest/globals";
import { phoneFormatter } from "../phone-formatter";

function dispatchInputChangeEvent(input: HTMLInputElement, value: string) {
  input.value = value;
  const changeEvent = new Event("change");
  input.dispatchEvent(changeEvent);
}

describe("Phone formatter", () => {
  let phoneInput: HTMLInputElement;

  beforeEach(() => {
    phoneInput = document.createElement("input");
    phoneInput.id = "phone";
    phoneInput.value = "";
  });

  afterEach(() => {
    phoneInput.remove();
  });

  test("has ( if has more than one digit", () => {
    phoneInput.addEventListener("change", (event) => {
      phoneFormatter(event);

      const phoneSymbols = phoneInput.value.split("");
      const hasOpeningBracket = phoneSymbols.includes("(");

      expect(hasOpeningBracket).toBe(true);
    });

    dispatchInputChangeEvent(phoneInput, "11");
    phoneInput.remove();
  });

  test("has ( and ) if has more than 4 digits", () => {
    phoneInput.addEventListener("change", (event) => {
      phoneFormatter(event);

      const phoneSymbols = phoneInput.value.split("");
      const hasOpenBracket = phoneSymbols.includes("(");
      const hasCloseBracket = phoneSymbols.includes(")");

      expect(hasOpenBracket && hasCloseBracket).toBe(true);
    });

    dispatchInputChangeEvent(phoneInput, "11111");
    phoneInput.remove();
  });

  test("has exact 16 symbols when provided correct length in digits", () => {
    phoneInput.addEventListener("change", (event) => {
      phoneFormatter(event);
      setTimeout(() => expect(phoneInput.value).toHaveLength(16), 200);
    });

    dispatchInputChangeEvent(phoneInput, "111111111111");
    phoneInput.remove();
  });

  test("has plus sign", () => {
    phoneInput.addEventListener("change", (event) => {
      phoneFormatter(event);

      const phoneSymbols = phoneInput.value.split("");
      const hasPlusSign = phoneSymbols.includes("+");

      expect(hasPlusSign).toBe(true);
    });

    dispatchInputChangeEvent(phoneInput, "123");
    phoneInput.remove();
  });

  test("match pattern", () => {
    phoneInput.addEventListener("change", (event) => {
      phoneFormatter(event);
      expect(phoneInput.value).toMatch(/^[+][0-9]{1}[(][0-9]{3}[)][-s.0-9]*$/g);
    });

    dispatchInputChangeEvent(phoneInput, "99999999999");
    phoneInput.remove();
  });
});
