import { classNamesFormatter } from "../class-names-formatter";
import { describe, expect, test } from "@jest/globals";

describe("Class names formatter", () => {
  test("inlines list of classes", () => {
    expect(classNamesFormatter("class1", ["class2", "class3"])).toBe(
      "class1 class2 class3"
    );
  });

  test("adds conditional classes", () => {
    expect(
      classNamesFormatter("class1", [], {
        class2: false,
        class3: true,
      })
    ).toBe("class1 class3");
  });

  test("compound class names", () => {
    expect(
      classNamesFormatter("class1", ["class2", "class3"], {
        class4: false,
        class5: true,
      })
    ).toBe("class1 class2 class3 class5");
  });

  test("do not modify if not additional args provided", () => {
    expect(classNamesFormatter("class1")).toBe("class1");
  });
});
