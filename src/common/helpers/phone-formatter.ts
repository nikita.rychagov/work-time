export function phoneFormatter(event: any) {
  if (!event.target.value.length) {
    return;
  }
  const input = event.target.value.replace(/\D/g, "").substring(0, 12);
  const countryCode = input.substring(0, 1);
  const areaCode = input.substring(1, 4);
  const middle = input.substring(4, 7);
  const two = input.substring(7, 9);
  const end = input.substring(9, 12);

  if (input.length > 9) {
    event.target.value = `+${countryCode}(${areaCode})${middle}-${two}-${end}`;
  } else if (input.length > 7) {
    event.target.value = `+${countryCode}(${areaCode})${middle}-${two}`;
  } else if (input.length > 4) {
    event.target.value = `+${countryCode}(${areaCode})${middle}`;
  } else if (input.length > 1) {
    event.target.value = `+${countryCode}(${areaCode}`;
  } else if (input.length > 0) {
    event.target.value = `+${countryCode}`;
  }
}
