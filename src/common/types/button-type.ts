export type ButtonType = "button" | "submit" | "reset" | undefined;

export enum ButtonTypeEnum {
  BUTTON = "button",
  SUBMIT = "submit",
  RESET = "reset",
}
