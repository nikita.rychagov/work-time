import "./primary-link.css";
import {ReactNode} from "react";

export function PrimaryLink({
                                children,
                                url,
                                isNewTab,
                                classNames,
                            }: {
    children: ReactNode;
    url: string;
    isNewTab?: boolean;
    classNames?: string[];
}) {
    return (
        <a
            className={`${classNames?.length ? classNames?.join(" ") : ''}  primary-link`}
            target={isNewTab ? "_blank" : "_self"}
            href={url}
        >
            {children}
        </a>
    );
}
