import { useFormContext } from "react-hook-form";
import "./input.css";
import { useEffect, useState } from "react";
import { classNamesFormatter } from "@/common/helpers";

export function Input({
  name,
  placeholder,
  validator,
  type = "text",
  onKeyDown,
  value,
  label,
  maxLength,
}: {
  name: string;
  validator: Record<string, any>;
  errors?: any;
  placeholder?: string;
  type?: string;
  onKeyDown?: any;
  value?: any;
  label: string;
  maxLength?: number;
}) {
  const {
    register,
    trigger,
    formState: { isDirty, errors, dirtyFields },
  } = useFormContext();

  const triggerValidation = () => trigger(name);

  const [hasError, setHasError] = useState(false);

  useEffect(() => {
    if (errors[name]) {
      setHasError(
        Object.keys(dirtyFields).includes(name) && Boolean(errors[name])
      );
    }
  }, [isDirty, errors]);

  const inputClassNames = classNamesFormatter("input", [], {
    "error-placeholder": Boolean(errors[name]),
    "after-error": Boolean(errors[name]),
  });

  return (
    <>
      <div className={"input-container"}>
        <div className={"input-content"}>
          <label className={"input-label"} htmlFor={name}>
            {label}
          </label>
          <input
            {...register(name, validator)}
            placeholder={placeholder}
            name={name}
            id={name}
            type={type}
            value={value}
            maxLength={maxLength}
            onBlur={triggerValidation}
            className={inputClassNames}
            onKeyDown={onKeyDown}
          />
          {Boolean(errors[name]) ? (
            <div className={"input-error"}>{`${errors[name]?.message}`}</div>
          ) : null}
        </div>
      </div>
    </>
  );
}
