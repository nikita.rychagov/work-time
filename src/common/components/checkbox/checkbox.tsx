import "./checkbox.css";
import { FormEvent } from "react";

export function Checkbox({
  onChange,
  checked,
}: {
  onChange: (checked: boolean) => void;
  checked?: boolean;
}) {
  const onChangeValue = ({ currentTarget }: FormEvent<HTMLInputElement>) => {
    onChange(currentTarget.checked);
  };
  return (
    <input
      className="checkbox"
      checked={checked}
      onChange={onChangeValue}
      type="checkbox"
    />
  );
}
