import { classNamesFormatter } from "@/common/helpers";
import { ButtonTypeEnum } from "@/common/types";
import "./button.css";
import { ReactNode } from "react";

export function Button({
  children,
  type = ButtonTypeEnum.SUBMIT,
  color = "primary",
  disabled = false,
  classNamesList,
  onClick,
  value,
}: {
  children: ReactNode;
  type?: ButtonTypeEnum;
  color?: string;
  classNamesList?: string[];
  onClick?: any;
  disabled?: boolean;
  show?: boolean;
  value?: any;
}) {
  const componentClassNames = {
    mainBtn: classNamesFormatter("main-btn", classNamesList, {
      "primary-btn": color === "primary",
      "white-btn": color === "white",
    }),
  };

  const handleClick = (event: any) => {
    if (type === "submit") return;
    event.preventDefault();
    onClick();
  };

  return (
    <>
      <button
        type={type}
        className={componentClassNames.mainBtn}
        onClick={handleClick}
        disabled={disabled}
        value={value}
      >
        {children}
      </button>
    </>
  );
}
