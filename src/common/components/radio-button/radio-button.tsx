import styles from "./radio-button.module.css";

export function RadioButton({
  label,
  onChange,
  name,
  value,
  checked,
}: {
  label: string;
  name: string;
  value: any;
  checked: boolean;
  onChange: any; // FIXME: Add type
}) {
  return (
    <div className={styles.container}>
      {" "}
      <label className={styles.item}>
        <input
          className={styles.input}
          type="radio"
          name={name}
          value={value}
          checked={checked}
          onChange={onChange}
        ></input>
        <span className={styles.label}>{label}</span>
      </label>
    </div>
  );
}
